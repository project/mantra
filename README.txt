
Description:
---------------------------
The Mantra module makes it fun and easy to change your site slogan to whatever catchphrase, tagline or snowclone that come to mind. Installing this module creates a"mantra" content type and changes the site slogan to the title of the most recently created "mantra" post.

See the accompanying INSTALL.txt for instructions on installing this module.



Notes:
---------------------------
Mantra gives anyone who can post a mantra the ability to change the site slogan without being given the 'administer site configuration' permission. It's important that only trusted users are given a role with the 'create mantra content' permission, so be sure to create another role for trusted users (say, an "admin" or "infrastructure" role) and only allow this role to post mantras.



Author:
---------------------------
Christefano
http://drupal.org/user/76901

This module was developed for Exaltation of Larks and its clients.